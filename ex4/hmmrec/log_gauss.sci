function y = log_gauss(X, model)
  
  nstate = size(model.state,2);
  [dim, numframe] = size(X);
  y = zeros(nstate, numframe);
  
  for s=1:nstate,
    state = model.state(1,s).entries;
    Mean = state.Mean;
    Cov  = state.Cov;
    
    dist = mahalan(X, Mean, Cov);
    y(s,:) = -0.5 * dist - 0.5 * log((2*%pi)^dim * det(Cov));
  end

  return;
  
endfunction

