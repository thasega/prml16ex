function data = load_2dvf(filename)

Fid = mopen(filename, 'r');
[N,X,Y,Z] = mfscanf(%inf, Fid, '%f%f%f');
mclose(Fid);

data.X = [X Y]';
data.y = Z';

return;

endfunction

