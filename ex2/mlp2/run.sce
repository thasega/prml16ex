clear;

// for scilab-4.*.*
//getf('.\cerror.sci');
//getf('.\load_2dvf.sci');
//getf('.\mlp.sci');
//getf('.\predictMLP.sci');

// for scilab-5.*.*
exec('.\cerror.sci');
exec('.\load_2dvf.sci');
exec('.\logistic.sci');
exec('.\mlp.sci');
exec('.\predictMLP.sci');
exec('.\visualize.sci');

rand('seed',1.005D+09);

//--------------------------------------------------
// Load formant data of five vowels
// - train_5vf.txt: training data
// - recog_5vf.txt: test data
trn_5vf = load_2dvf('.\train_5vf.txt');
tst_5vf = load_2dvf('.\recog_5vf.txt');

//--------------------------------------------------
// Generate training data (of "A" and "I")
idx1 = find(trn_5vf.y==4); // index of "A"
idx2 = find(trn_5vf.y==5); // index of "I"
len1 = length(idx1);
len2 = length(idx2);
trn.X = trn_5vf.X(:,[idx1 idx2]);
trn.y(:,1:len1)          = 0; // class-1
trn.y(:,len1+1:len1+len2)= 1; // class-2

//--------------------------------------------------
// Generate testing data (of "A" and "I")
idx1 = find(tst_5vf.y==4); // index of "A"
idx2 = find(tst_5vf.y==5); // index of "I"
len1 = length(idx1);
len2 = length(idx2);
tst.X = tst_5vf.X(:,[idx1 idx2]);
tst.y(:,1:len1)           = 0; // class-1
tst.y(:,len1+1:len1+len2) = 1; // class-2

// ----------------------------
// Data normalization
trn.X=(trn.X-mean(trn.X,'c')*ones(1,size(trn.X,2))) ./ sqrt(variance(trn.X,'c')*ones(1,size(trn.X,2)));
tst.X=(tst.X-mean(tst.X,'c')*ones(1,size(tst.X,2))) ./ sqrt(variance(tst.X,'c')*ones(1,size(tst.X,2)));
//----------------------

// Neural network (MLP)
options.eta     = 1;    // Number of maximum iterations
options.nhidden = 10;   // Number of hidden nodes
options.tmax    = 10000; // Number of maximum iterations
model = mlp(trn, options);

//--------------------------------------------------
// Classify testing data
// - ypred: Predicted labels. [1 x num_data]
// - dfce: Values of discriminant function [1 x num_data]
[ypred, dfce] = predictMLP(model, tst.X);

//--------------------------------------------------
// Evaluate testing error
err = cerror(ypred, tst.y)

//--------------------------------------------------

// Visualization -- Scilab 5.X
plot(tst.X(1,1:len1),tst.X(2,1:len1),'bx');
plot(tst.X(1,len1+1:len1+len2),tst.X(2,len1+1:len1+len2),'ro');
legend('A','I','hyperplane');

// Visualization -- Scilab 4.X (Old Graphics Mode)
//plot2d(tst.X(1,1:len1),tst.X(2,1:len1),style=-1);
//plot2d(tst.X(1,len1+1:len1+len2),tst.X(2,len1+1:len1+len2),style=-2);
visualize(model);
legend('A','I','hyperplane');
printf("=============================\n")
printf("Phone error rate: %.2f%%\n", err*100);
printf("=============================\n")
return;

// End: pr2014ex02.sce

