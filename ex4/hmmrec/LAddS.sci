function sumLogL = LAddS(LogL1, LogL2)
  
  if LogL1 > LogL2,
    sumLogL = LogL1 + log(1 + exp(LogL2 - LogL1));
  else
    sumLogL = LogL2 + log(1 + exp(LogL1 - LogL2));
  end
    
  return;
    
endfunction

