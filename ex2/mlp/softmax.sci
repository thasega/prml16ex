function y=softmax(x)
    ex = exp(x);
    y = ex ./ repmat(sum(ex,1),size(ex,1),1);
endfunction    
