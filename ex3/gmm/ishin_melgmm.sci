function model=melgmm(X,Alpha,cov_type)
// MELGMM Maximizes Expectation of Log-Likelihood for Gaussian mixture.
// 
// Synopsis:
//  model = melgmm(X,Alpha)
//  model = melgmm(X,Alpha,cov_type)
// 
// Description:
//  model = melgmm(X,Alpha) maximizes expectation of log-likelihood 
//  function for Gaussian mixture model
//                        
//   (Mean,Cov,Prior) =  argmax  F(Mean,Cov,Prior)
//                    Mean,Cov,Prior 
//
//  where
//   F = sum sum Alpha(j,i)*log(pdfgauss(X(:,i),Mean(:,y),Cov(:,:,y)))
//        y   i 
//
//  The solution is returned in the structure model with fields
//  Mean [dim x ncomp], Cov [dim x dim x ncomp] and Prior [1 x ncomp].
//
//  model = melgmm(X,Alpha,cov_type) specifies covariance matrix:
//   cov_type = 'full'      full covariance matrix (default)
//   cov_type = 'diag'      diagonal covarinace matrix
//   cov_type = 'spherical' spherical covariance matrix
//
// Input:
//  X [dim x num_data] Data sample.
//  Alpha [ncomp x num_data] Distribution of hidden state given sample.
//  cov_type [string] Type of covariacne matrix (see above).
//
// Output:
//  model [struct] Gaussian mixture model:
//   .Mean [dim x ncomp] Mean vectors.
//   .Cov [dim x dim x ncomp] Covariance matrices.
//   .Prior [1 x ncomp] Distribution of hidden state.
//
// See also 
//  EMGMM, MLCGMM.
//

// About: Statistical Pattern Recognition Toolbox
// (C) 1999-2003, Written by Vojtech Franc and Vaclav Hlavac
// <a href="http://www.cvut.cz">Czech Technical University Prague</a>
// <a href="http://www.feld.cvut.cz">Faculty of Electrical Engineering</a>
// <a href="http://cmp.felk.cvut.cz">Center for Machine Perception</a>

// Modifications:
// 30-apr-2004, VF
// 19-sep-2003, VF
// 27-feb-2003, VF

// Processing of input arguments 
//----------------------------------------
if argn(2) < 3, cov_type = 'full'; end
[dim, num_data] = size( X );
  
// ------------------------------------
ncomp = size(Alpha,1);

model.Mean = zeros(dim,ncomp);
model.Cov = cell(1, ncomp);


for i=1:ncomp,

  nconst = sum( Alpha(i,:) );
  if ~nconst,
    model.Mean(:,i) = %nan * ones(dim,1);
    model.Cov(1,i).entries = %nan * ones(dim, dim);
    model.Prior(i) = 0;
  else
    model.Mean(:,i) = X * Alpha(i,:)'./nconst;///// YOU HAVE TO WRITE ... 平均ベクトルの推定値
    XC = X - model.Mean(:,i) * ones(1, num_data);
    disp(size(XC))
    select cov_type,
      case 'full'
        model.Cov(1,i).entries = (ones(2,1) * Alpha(i,:)) .* XC * XC'./nconst; //// YOU HAVE TO WRITE ... 全共分散行列の推定
        
      case 'diag'
        cov_all = (ones(2,1) * Alpha(i,:)) .* XC * XC' ./ nconst;
        model.Cov(1,i).entries = diag(diag(cov_all));///// YOU HAVE TO WRITE ... 対角共分散行列の推定 
      else
        error('Wrong cov_type.');
    end
    
//    model.Prior(i) = nconst / num_data;///// YOU HAVE TO WRITE ... 混合重みの推定
  end
end  
  
return;

endfunction

