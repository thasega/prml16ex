function [ypred, dfce]  = predictMLP(model, X)
    dfce = logistic(logistic([model.W1; model.b1]' * [X;ones(1,size(X,2))])'*model.W2)';
    ypred = round(dfce);
endfunction
