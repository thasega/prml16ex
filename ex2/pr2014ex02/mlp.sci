function model = mlp(data, options, init_model)
// Multi Perceptron algorithm with sigmoid function to train binary linear classifier. 
//
// Synopsis:
//  model = perceptron(data)
//  model = perceptron(data,options)
//  model = perceptron(data,options,init_model)
//
// Input:
//  data [struct] Labeled (binary) training data. 
//   .X [dim x num_data] Input vectors.
//   .y [1 x num_data] Labels (1 or 2).
//
//  options [struct] 
//   .tmax [1x1] Maximal number of iterations (default tmax=inf).
//     If tmax==-1 then it does not perform any iteration but returns only 
//     index of the point which should be used to update linear rule.
//  
//  init_model [struct] Initial model; must contain items
//    .W, .b and .t (see above).
//
// (C) 2014, Written by Naohiro Tawara (tawara@pcl.cs.waseda.ac.jp)
//

[dim, num_data] = size(data.X); // dimension and num of data

// Process input arguments 
// -----------------------------
if argn(2) < 2, 
  options = [];
end

if ~mtlb_isfield(options,'tmax'),
  options.tmax = %inf;
end
if ~mtlb_isfield(options,'eta'),
  options.eta = 0.00;
end
if ~mtlb_isfield(options,'hdidden'),
  options.nhidden = 10;
end

eta        = options.eta;
num_hidden = options.nhidden;
tmax       = options.tmax;

if argn(2) < 3,
  // create init model
  model.W1 =  -1 + rand(dim, num_hidden) * 2; //隠れ層の重み 2×10
  model.b1 =  -1 + rand(1,   num_hidden) * 2; // 1 × 10
  model.W2 =  -1 + rand(num_hidden, 1)   * 2; //出力層の重み 10×1
else
  // take init model from input
  model = init_model;
end


    

if ~mtlb_isfield(model,'t'), 
  model.t = 0;
end

model.exitflag    = 0;
model.last_update = 0;

// augument input vector
// Add one constant coordinates to the data and swap
// points from the second class along the origin.
dim    = dim + 1;
data.X = [data.X; ones(1,num_data)]; // 3 × 400

// main loop 
// -----------------------------------
olderr = %inf;
    model.H = zeros(num_hidden,num_data);
    model.Y = zeros(1,num_data);
    deltaY = zeros(1,num_data);
    deltaH = zeros(num_hidden,num_data);
while tmax > model.t & model.exitflag == 0
    // =============================
    // TODO
		// forward propagation 
    model.H = logistic([model.W1; model.b1]' * data.X); 
    model.Y = logistic([model.W2]' * model.H); 
    
    //calcurate delta
    deltaY = (data.y-model.Y) .* model.Y .* (1 - model.Y);
    
    deltaH = repmat(deltaY,num_hidden,1) .* repmat(model.W2,1,num_data) .* model.H .* (1 - model.H); 

    //back propagation
    model.W2 = model.W2 + eta/num_hidden .* (deltaY * model.H')';
    model.W1 = model.W1 + eta/num_hidden .* (deltaH * data.X(1:2,:)')' ;
    model.b1 = model.b1 + eta/num_hidden .* (deltaH * data.X(3,:)')' ;
    
    
 
    // =============================
    
    model.t = model.t + 1;
    err = sum(data.y-model.Y) ./num_data * eta;
    delta_err = abs(olderr - err);
    printf('%d: Error: %f(delta: %f) Old: %f\n',model.t, err, delta_err,olderr);
    if(delta_err < 0.00001) then
        model.exitflag = 1;
    end
    olderr = err;
end

endfunction
