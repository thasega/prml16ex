function a = viterbi(lobsprob, model)
  
  // model.trans(s,1) : probability of same state transition of s-th state
  // model.trans(s,2) : probability of next state transition from s-th state to s+1-th state
  
  ns = size(model.state,2); // Number of states
  nf = size(lobsprob,2);     // Number of frames
  
  a = -%inf * ones(ns,nf);
  a(1,1) = 0.0 + 0.0 + lobsprob(1,1);
  max_ns = 1;
  for t=2:nf,
    a(1,t) = a(1,t-1) + log(model.trans(1,1)) + lobsprob(1,t);
    if t >= ns,
      max_ns = ns;
    else
      max_ns = max_ns + 1;
    end
    for s=2:max_ns,
      sumLogPr = -%inf;
      sumLogPr = a(s-1, t-1) + log(model.trans(s-1, 2));
            // YOU HAVE TO WRITE ... (for next state transition)
      sumLogPr2 =  max(a(s, t-1) + log(model.trans(s, 1)), sumLogPr);
            // YOU HAVE TO WRITE ... (for same state transition)
      a(s,t) = sumLogPr2 + lobsprob(s, t);
            // YOU HAVE TO WRITE ...
    end
  end
  
  return 

endfunction
