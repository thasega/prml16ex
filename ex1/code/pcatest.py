# -*- coding: utf-8 -*-
from matplotlib import pylab as plt
from numpy.random import randn
from scipy import array, ceil, dot, float64, floor, matrix, sqrt, zeros
from scipy.linalg import cholesky, eig, norm, solve, inv
import numpy as np
from numpy.random import multivariate_normal

"""
Maximum Variance criterion
"""
def pca(data, base_num = 1):
    """
    purpose:calculate PCA axis
    input : all data
    output : base axis vector
    """
    N, dim = data.shape
    
    data_m = data.mean(0)
    data_new = data - data_m
    ### データ行列の共分散行列
    cov_mat = dot(data_new.T, data_new) / float(N)
    ### 固有値・固有ベクトルを計算
    l, vm = eig(cov_mat)
    ### 固有値が大きい順に並び替え
    axis = vm[:, l.argsort()[- min(base_num, dim) :][:: -1]].T

    return axis

def lda(data, data1, data2):
    """
    purpose : calculate LDA
    input : data=all data, data1=class1, data2=class2
    output : base vector 
    """
    N1, dim1 = data1.shape
    N2, dim2 = data2.shape

    data1_m = data1.mean(0)
    data2_m = data2.mean(0)
    data_m = data.mean(0)
    data1_new = data1 - data1_m
    data2_new = data2 - data2_m
    data1_mea = data1_m - data_m
    data2_mea = data2_m - data_m

    data1_mea=np.matrix(data1_mea)
    data2_mea=np.matrix(data2_mea)

    data1_sca = dot(data1_new.T, data1_new)
    data2_sca = dot(data2_new.T, data2_new)
    data1_mean = dot(data1_mea.T, data1_mea)
    data2_mean = dot(data2_mea.T, data2_mea)

    ### Swを計算する
    S_w = data1_sca + data2_sca
    ### Sbを計算する
    S_b = N1 * data1_mean + N2 * data2_mean

    S_w_invert = inv(S_w)
    S_wS_b = S_w_invert.dot(S_b)
    ### 固有値を計算する
    Myeig = np.linalg.eig(S_wS_b)
    ### 固有値で最大の固有ベクトルを返す
    w = Myeig[1][np.argmax(Myeig[0])].T

    return np.array(w)

def generate_data():
    """init and generate data"""
    mu1 = [5, 4]
    mu2 = [4, 5]
    cov = [[1, 0.9],[0.9, 1]]
    data1 = multivariate_normal(mu1, cov, 100)
    data2 = multivariate_normal(mu2, cov, 100)
    data = np.r_[data1, data2]

    ### PCA
    pc_base = pca(data, base_num = 1)[0]
    ### LDA
    ld_base = lda(data, data1, data2)

    plot_data(data1 ,data2, data, pc_base, ld_base)

def plot_data(data1, data2, data, pc_base, ld_base):
    plt.figure(facecolor='w')
    plt.scatter(data1[:, 0], data1[:, 1], marker='x', color='b', label="class-1")
    plt.scatter(data2[:, 0], data2[:, 1], marker='.', color='g', label="class-2")

    pc_line = array([1, 8.]) * (pc_base[1] / pc_base[0])
    ld_line = array([1, 8.]) * (-1 * ld_base[0] / ld_base[1]) + array([9, 9])

    plt.figtext(0.6,0.3,"$N_1$",size=20)
    plt.figtext(0.3,0.6,"$N_2$",size=20)
     
    plt.xlabel('$x$',size=20)
    plt.ylabel('$y$',size=20)
    plt.legend(loc='lower right')

    plt.plot([1, 8], pc_line, "r")
    plt.plot([3, 6], ld_line, "b")
    plt.savefig("pcalda.eps")
    plt.show()

if __name__ == "__main__":
    generate_data()
