function visualize(model)
x=[-2:0.1:2];
y=[-2:0.1:2];
n = size(x,2);
[X Y] = ndgrid(x,y);
P = predictMLP(model, [X(:), Y(:)]');
contour2d(x, y,matrix(P, n, n),1);
endfunction
