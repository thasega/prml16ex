function a = forward(lobsprob, model)
  
  ns = size(model.state,2); // Number of states
  nf = size(lobsprob,2);    // Number of frames
  
  a = -%inf * ones(ns,nf);
  a(1,1) = 0.0 + 0.0 + lobsprob(1,1);
  max_ns = 1;
  for s = 1:ns
      for f = s:nf
          if s==1 & f==1 then
              a(s,f) = a(1, 1);
          elseif s==1 then
              a(s,f) = a(s, f-1) + log(model.trans(s,1)) + lobsprob(s, f);
          elseif f==s then
              a(s,f) = a(s-1, f-1) + log(model.trans(s-1,2)) + lobsprob(s, f);
          else
              a(s,f) = LAddS(a(s, f-1) + log(model.trans(s,1)), a(s-1, f-1) + log(model.trans(s-1,2))) + lobsprob(s, f);
          end
      end
  end

  return 

endfunction
