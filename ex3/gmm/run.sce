// run.sce: 
// sample program for vowel classification 
// using Gaussian mixture models 
// trained by EM algotithm

clear;

exec('./randperm.sci');
exec('./knnclass.sci');
exec('./knnrule.sci');
exec('./mlcgmm.sci');
exec('./mahalan.sci');
exec('./pdfgauss.sci');
exec('./log_pdfgauss.sci');
exec('./melgmm.sci');
exec('./cmeans.sci');
exec('./LAdd.sci');
exec('./emgmm.sci');
exec('./log_pdfgmm.sci');
exec('./cerror.sci');
exec('./load_2dvf.sci');

//--------------------------------------------------
// Load formant data of five vowels
// - train_5vf.txt: training data
// - recog_5vf.txt: test data
//--------------------------------------------------
trn_5vf = load_2dvf('./train_5vf.txt');
tst_5vf = load_2dvf('./recog_5vf.txt');

//--------------------------------------------------
// Training of GMM using ML estimation by Em algorithm
//
// Options:
//  .ncomp: Number of Gaussian Components (default: 2)
//  .tmax: Maximum number of iterations (default: %inf)
//  .init: 'random' use random initial model (default);
//         'cmeans' use K-means to find initial model.
//  .cov_type: Type of estimated covariance matrices
//  .verb: If 1 then processing details are displayed.
//--------------------------------------------------
model = cell(1,5);

options.ncomp = 1;
options.tmax = 20;
options.init = 'cmeans';
//options.cov_type = 'full';
options.cov_type = 'diag';
options.verb = 0;

for i = 1:5,
  idx = find(trn_5vf.y==i);
  model(1,i).entries = emgmm(trn_5vf.X(:,idx), options);
end

//--------------------------------------------------
// Classify testing data
//--------------------------------------------------
tst_data = tst_5vf.X;
[dim, num_tst_data] = size(tst_data);
for i = 1:5,
  dfce(i,:) = log_pdfgmm(tst_data, model(1,i).entries);
end

//--------------------------------------------------
// Evaluate testing error
//--------------------------------------------------
[max_value, max_idx] = max(dfce,'r');

error_rate = cerror(tst_5vf.y, max_idx,1)
error_rate = cerror(tst_5vf.y, max_idx,2)
error_rate = cerror(tst_5vf.y, max_idx,3)
error_rate = cerror(tst_5vf.y, max_idx,4)
error_rate = cerror(tst_5vf.y, max_idx,5)
error_rate = cerror(tst_5vf.y, max_idx)
//--------------------------------------------------
//          End: run.sce
//--------------------------------------------------
